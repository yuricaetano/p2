##!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

def standardize_names(character_name):
    a = character_name
    a = a.strip(' ')
    a = a.replace(' ', '-')
    return a

# Eu não sabia que nao era pra usar array, conforme feedback na atividade retirei e funcionou.

def standardize_title(title):
  result = []
  a = title
  a = a.lower()
  a = a.split()
  
  for item in a:
    result.append(item.capitalize())

  separator = (' ')
  result = separator.join(result)
  
  return result

#Essa achei fácil, foi só colocar a primeira letra maiuscula com capitalize
text = """ A famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma lista com números de
telefone de pessoas que morreram recentemente. é uma coisa assustadora, considerando que os nomes das
poucas pessoas vivas presentes na lista estão assinalados em vermelho com uma cruz. O da própria Constance é um deles."""

temqueser = """ A famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma lista com números de
telefone de pessoas que morreram recentemente. É uma coisa assustadora, considerando que os nomes das
poucas pessoas vivas presentes na lista estão assinalados em vermelho com uma cruz. O da própria Constance é um deles."""

def standardize_text(text):
  texto = []
  a = text
  a = a.split('. ')
  for item in a:
    b = item
    b = list(b)
    b[0] = b[0].capitalize()
    separator = ""
    b = separator.join(b)

    texto.append(b)

  separator = '. '

  texto = separator.join(texto)
  print(texto)

  return texto

resultado = standardize_text(text)




# Retirei o array de herois e consegui fazer, alem disso nao sabia que nao iam poder ter os espaços extras no texto.

def title_creator(text):
    resultado = []
    a = text.split()

    for item in a:
      b = item.capitalize()
      resultado.append(b) 

    separator = ' '
    texto = separator.join(resultado)

    tamanho_total = len(text) + 40
    a = texto.center(tamanho_total, '-')

    return a 

# Consegui dando split, e depois join com o separator.

text_of_blog_a = """
na Londres do pós-guerra, a escritora     Juliet tenta encontrar uma   trama para seu novo livro. ela recebe ajuda
por meio de uma     carta de um      desconhecido, um nativo da ilha de Guernsey, em cujas mãos havia chegado
um livro    que há tempos tinha pertencido    a Juliet.
"""
text_of_blog_b = """
O romance "Cinco Quartos de Laranja" é como   um vinho intenso e delicado.    usando metáforas culinárias,
personagens peculiares   e acontecimentos sobrenaturais,      Harris cria uma história complexa e      bela ao
mesmo tempo.
"""

def text_merge(text_of_blog_a, text_of_blog_b):
  list_words = []
  text = text_of_blog_a + text_of_blog_b

  text = text.split()
  separator = (' ')
  text = separator.join(text)
  
  text = text.split('. ')
  for item in text:
    a = item.capitalize()
    list_words.append(a)

  separator2 = ('. ')
  retorno = separator2.join(list_words)
  return retorno

# Dei strip para tirar os espaços, split para separar após o ponto, e join com o separator '. ' 