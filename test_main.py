##!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

from main import standardize_names, standardize_title, standardize_text, title_creator, text_merge

def test_standardize_names():

    resultado = standardize_names(" Batman     ")
    esperado = "Batman"
    assert resultado == esperado

    resultado = standardize_names("      Super Man")
    esperado = "Super-Man"
    assert resultado == esperado

def test_standardize_title():

    resultado = standardize_title("cinco quartos de laranja")
    esperado = "Cinco Quartos De Laranja"
    assert resultado == esperado

def test_standardize_text():

    text = """ A famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma lista com números de
telefone de pessoas que morreram recentemente. é uma coisa assustadora, considerando que os nomes das
poucas pessoas vivas presentes na lista estão assinalados em vermelho com uma cruz. O da própria Constance é um deles."""

    text_to_be = """ A famosa atriz Constance Rattigan recebe uma encomenda desagradável: uma lista com números de
telefone de pessoas que morreram recentemente. É uma coisa assustadora, considerando que os nomes das
poucas pessoas vivas presentes na lista estão assinalados em vermelho com uma cruz. O da própria Constance é um deles."""

    resultado = standardize_text(text)
    resultado = resultado.encode('utf-8')
    esperado = text_to_be.encode('utf-8')
    assert resultado == esperado


 
 
